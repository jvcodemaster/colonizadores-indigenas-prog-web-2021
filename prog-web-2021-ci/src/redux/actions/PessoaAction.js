import PessoaService from "../../service/PessoaService";


// padronizar os nomes das actions
export const PESSOA_ACTIONS = {
    LISTAR: "PESSOA_LISTAR",
    BUSCAR: "PESSOA_BUSCAR",
    SALVAR: "PESSOA_SALVAR",
    ALTERAR: "PESSOA_ALTERAR",
    EXCLUIR: "PESSOA_EXCLUIR",
}


export function listarPessoa() {

    return function( callback ){

        PessoaService.listar()
        .then(response => {

            callback({
                type: PESSOA_ACTIONS.LISTAR,
                content: response.data
            })

        })
        .catch( error => { console.log("ERROR = ", error) } )  

    }

}

export function buscarPessoa(id) {
    return function (callback) {
        PessoaService.buscarPeloId(id)
            .then( response => {
                callback({
                    type: PESSOA_ACTIONS.BUSCAR,
                    content: response.data
                })
            })
            .catch( error => { console.log("ERROR = ", error) }  )
    }
}

export function salvarPessoa(pessoa) {
    return function (callback) {
        PessoaService.salvar(pessoa)
            .then( response => {
                const action = pessoa.id == undefined ? PESSOA_ACTIONS.SALVAR : PESSOA_ACTIONS.ALTERAR;
                callback({
                    type: action,
                    content: response.data
                }) 
            })
            .catch( error => console.log("ERROR = ", error))
    }
}

export function excluirPessoa(id) {
    return function( callback ){
        PessoaService.excluir(id)
            .then(callback({
                type: PESSOA_ACTIONS.EXCLUIR,
                content: id
            }))
            .catch( error => { console.log("ERROR = ", error) }  )
    }
}

export function setPessoaItem(pessoa){
    return function( callback ){
        callback({
            type: PESSOA_ACTIONS.BUSCAR,
            content: pessoa
        })
    }
}