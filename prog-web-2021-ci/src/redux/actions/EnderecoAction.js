import EnderecoService from "../../service/EnderecoService";

// padronizar os nomes das actions
export const ENDERECO_ACTIONS = {
    LISTAR: "ENDERECO_LISTAR",
    BUSCAR: "ENDERECO_BUSCAR",
    SALVAR: "ENDERECO_SALVAR",
    ALTERAR: "ENDERECO_ALTERAR",
    EXCLUIR: "ENDERECO_EXCLUIR",
}

export function listarEndereco(pessoaId) {

    return function( callback ){
        EnderecoService.listar(pessoaId)
        .then(response => {
            callback({
                type: ENDERECO_ACTIONS.LISTAR,
                content: response.data
            })
        })
        .catch( error => { console.log("ERROR = ", error) } )  
    }
}