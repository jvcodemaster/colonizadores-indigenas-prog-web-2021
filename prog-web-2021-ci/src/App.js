import React from "react";
import logo from './logo.svg';
import './App.css';
import './Styles.css';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import {Provider} from "react-redux";
import store from "./redux/store"


import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Nav from 'react-bootstrap/Nav';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart,faUserCircle } from '@fortawesome/free-solid-svg-icons';

import PessoaFormulario from './views/pessoa/PessoaFormulario';
import PessoaLista from './views/pessoa/PessoaLista';
import Home from './Home';
import NotFound from './NotFound';

function App() {
  return (

      <Provider store={store}>

        <div className="App">
          <Router>

            
          <div className="header">
            <Container>
              <Row>
                <Col xs lg="2">
                  <img id="logo" alt="Logomarca" src={`${process.env.PUBLIC_URL}/img/logo.png`} />
                </Col>
                <Col md="auto">
                  <Nav>
                    <Nav.Item>
                      <Nav.Link href="/home"> Início</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link href="/produtos">Conteúdos</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link href="/home">Informações</Nav.Link>
                    </Nav.Item>
                  </Nav>
                </Col>
                <Col xs lg="1">
                  <Container>
                    <Row>
                      <Col></Col>
                    </Row>
                    <Row>
                      <Col></Col>
                    </Row>
                  </Container>
                </Col>
                <Col xs lg="1">
                  <FontAwesomeIcon className="user-login" icon={faUserCircle} />
                </Col>
              </Row>
            </Container>
          </div>

          <Container>
            <Row>
              <Col xs={3}>
              <Nav className="flex-column menu-vertical">
                <Link to="/" className="bg-light-gray nav-link">Início</Link>
                <Link to="/pessoa/lista" className="bg-light-gray nav-link">Usuário</Link>
              </Nav>

              </Col>
              <Col xs={9}>
                
                <Switch>
                  <Route exact path="/"><Home  /></Route>
                  <Route path="/pessoa/lista"> <PessoaLista /> </Route>
                  <Route exact path="/pessoa/formulario"> <PessoaFormulario /> </Route>
                  <Route exact path="/pessoa/formulario/:id"> <PessoaFormulario /> </Route>
                  <Route path="*"><NotFound /></Route>
                </Switch>


              </Col>
            </Row>

            

          </Container>

          <div className="bg-dark-grey">
            <Container>
              <Row  >
                <Col> Colonizers of Knowledge © All rights reserved 2021</Col>
              </Row>
            </Container>
          </div>

          </Router>

        </div>
      </Provider>
  );
}

export default App;
