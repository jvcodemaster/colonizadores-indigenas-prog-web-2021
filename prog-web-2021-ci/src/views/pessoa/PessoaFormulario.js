import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";

import {salvarPessoa, buscarPessoa, setPessoaItem} from "../../redux/actions/PessoaAction";
import {pessoaFilter} from "../../redux/filters/PessoaFilter"

import { Formik, Form, Field } from 'formik';
import {
    useHistory,
    useParams
  } from "react-router-dom";

function PessoaFormulario(props){
    
    /* HOOKS */
    let history = useHistory();
    let {id} = useParams();
    useEffect(() => {
        buscarPessoa();
    }, []);

    /* STATES DO COMPONENTE*/


    /* MÉTODOS REDUX */
    const buscarPessoa = () => {
        if(id != undefined){
            props.buscarPessoa(id);
        } else {
            props.setPessoaItem({});
        }
        
    }

    /* MÉTODOS DA VIEW */
    const handleSubmit = (values, {setSubmitting} ) => {
        props.salvarPessoa(values);
        setSubmitting(false);
        history.push("/pessoa/lista");
    }

    const mapStateToObject = () => {
        return {
            id: props.pessoaItem.id || undefined,
            nome: props.pessoaItem.nome || '',
            cpf: props.pessoaItem.cpf || '',
            img: props.pessoaItem.img || ''
        }
    }

    
    return(
        <div>
            <h1>Cadastro de Pessoa</h1>

            <Formik 
                initialValues={ mapStateToObject() }
                enableReinitialize
                onSubmit={ (values, {setSubmitting} ) => handleSubmit(values, {setSubmitting} ) }

            >

                {  ({ values, handleSubmit, isSubmitting }) => (

                    <Form onSubmit={handleSubmit}>

                        <div className="input-group mb-3">
                            <Field type="text" name="img" className="form-control" />
                            <label className="input-group-text" htmlFor="inputGroupFile02">Upload</label>
                        </div>

                        <div className="mb-3">
                            <label name="nome" htmlFor="exampleInputEmail1" className="form-label">Nome</label>
                            <Field type="text" name="nome" className="form-control" />
                        </div>

                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">CPF</label>
                            <Field type="text" name="cpf" className="form-control" />
                        </div>

                        <button type="submit" className="btn btn-primary" disabled={isSubmitting} >Salvar</button>
                    </Form>


                )}



            </Formik>


        </div>
    );
}

export default connect(pessoaFilter, {salvarPessoa, buscarPessoa,setPessoaItem})(PessoaFormulario);
