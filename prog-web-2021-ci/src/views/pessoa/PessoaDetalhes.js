import React, { useState, useEffect } from 'react';

import values from "lodash/values";

// HOOKS
import { useHistory,useParams} from "react-router-dom";

// ROUTER
import { BrowserRouter as Router, Link} from "react-router-dom";

//REDUX
import { connect } from "react-redux";
import {salvarPessoa, buscarPessoa, setPessoaItem} from "../../redux/actions/PessoaAction";
import {pessoaFilter} from "../../redux/filters/PessoaFilter";

import {listarEndereco} from "../../redux/actions/EnderecoAction";
import {enderecoFilter} from "../../redux/filters/EnderecoFilter";

// ESTILO 
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrashAlt, faUserCircle  } from '@fortawesome/free-solid-svg-icons';
import '../../Styles.css';
import Button from 'react-bootstrap/Button'

function PessoaDetalhes(props){

    let {id} = useParams();
    let history = useHistory();
    useEffect(() => {
        buscarPessoa();
    }, []);

    // MÉTODOS DO REDUX
    const buscarPessoa = () => {
        if(id != undefined){
            props.buscarPessoa(id);
            props.listarEndereco(id);
        } else {
            history.push("/pessoa/lista");
        }
        
    }

    return(
        <div>
            <h1> Detalhes da Pessoa</h1>

            <div className="container form-detalhes">

                <div className="row user-header">
                    <div className="col-3"><FontAwesomeIcon className="user-icon"  icon={faUserCircle} /></div>
                    <div className="col-9 user-name">Millys Fabrielle Araujo Carvalhaes</div>
                </div>

                <div className="row">
                    <div className="col-3 input-header">CPF:</div>
                    <div className="col-9 input-text">000.000.000-00</div>
                </div>

            </div>

            <br/>
            <h3>Endereços:</h3>
            <Link to={`/endereco/formulario/`} className="btn btn-primary">Novo</Link>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Logradouro</th>
                        <th scope="col">CEP</th>
                        <th scope="col">Operações</th>
                    </tr>
                </thead>
                <tbody>
                    {props.enderecoLista.map( (endereco, index) => {
                        return(
                            <tr key={index}>
                                <th scope="row">{endereco.id}</th>
                                <td>{endereco.logradouro}</td>
                                <td>casa y</td>
                                <td>
                                    <Link to={`/endereco/formulario/1`} className="btn btn-outline-dark mrl-10"><FontAwesomeIcon icon={faPencilAlt} /></Link>
                                    <Button  variant="outline-dark"><FontAwesomeIcon icon={faTrashAlt} /></Button>
                                </td>
                            </tr>
                        );
                    })}

                    
                </tbody>
            </table>




        </div>
    );
}

function mapStateToProps(state){

    const {pessoaState} = state;
    const {enderecoState} = state;

    return { 
        enderecoLista: values(enderecoState.enderecoLista),
        pessoaItem: pessoaState.pessoaItem
    }
}

export default connect(mapStateToProps, {salvarPessoa, buscarPessoa,setPessoaItem, listarEndereco})(PessoaDetalhes);