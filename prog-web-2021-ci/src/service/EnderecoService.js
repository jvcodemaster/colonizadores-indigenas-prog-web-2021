//import React from 'react';
import axios from 'axios'; 


const listar = (pessoaId) => {
    return axios.get( 'http://localhost:3004/enderecos?pessoaId='+pessoaId);
}

const buscarPeloId = (id) => {
    return axios.get( 'http://localhost:3004/enderecos/'+id);
}

const salvar = (endereco) => {
    if(endereco.id == undefined){
        return axios.post('http://localhost:3004/enderecos/',endereco)
    } 
    else {
        return axios.put('http://localhost:3004/endereco/'+endereco.id,endereco)
    }
}

const excluir = (id) => {
    return axios.delete( 'http://localhost:3004/pessoas/'+id);
}

export default {
    listar,
    buscarPeloId,
    salvar,
    excluir
}