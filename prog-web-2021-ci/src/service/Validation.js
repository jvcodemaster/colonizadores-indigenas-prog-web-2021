var g_UnFocusElementStyle = "";
var g_FocusBackColor = "#FFC";
var g_reEmail = /^[\w\.=-]+\@[\w\.-]+.[a-z]{2,4}$/;
var g_reCell = /^\d{10}$/;
var g_invalidFields = 0;

function initFormElements(sValidElems) {
	var inputElems = document.getElementsByTagName('textarea');
	for(var i = 0; i < inputElems.length; i++) {
		com_abhi.EVENTS.addEventHandler(inputElems[i], 'focus', highlightFormElement, false);
		com_abhi.EVENTS.addEventHandler(inputElems[i], 'blur', unHightlightFormElement, false);
	}
	/* Add the code for the input elements */
	inputElems = document.getElementsByTagName('input');
	for(var i = 0; i < inputElems.length; i++) {
		if(sValidElems.indexOf(inputElems[i].getAttribute('type') != -1)) {
			com_abhi.EVENTS.addEventHandler(inputElems[i], 'focus', highlightFormElement, false);
			com_abhi.EVENTS.addEventHandler(inputElems[i], 'blur', unHightlightFormElement, false);
		}
	}
	
	/* submit handler */
	
	
	com_abhi.EVENTS.addEventHandler(document.getElementById('form1'), 'submit' , validateAllfields, false);
	
	/* Add the default focus handler */
	document.getElementsByTagName('input')[0].focus();
	
	/* Add the event handlers for validation */
	com_abhi.EVENTS.addEventHandler(document.forms[0].firstName, 'blur', validateFirstName, false);
	com_abhi.EVENTS.addEventHandler(document.forms[0].email, 'blur', validateEmailAddress, false);
	com_abhi.EVENTS.addEventHandler(document.forms[0].address, 'blur', validateAddress, false);
	com_abhi.EVENTS.addEventHandler(document.forms[0].cellPhone, 'blur', validateCellPhone, false);
}

function highlightFormElement(evt) {
	var elem = com_abhi.EVENTS.getEventTarget(evt);
	if(elem != null) {
		elem.style.backgroundColor = g_FocusBackColor;
	}
}

function unHightlightFormElement(evt) {
	var elem = com_abhi.EVENTS.getEventTarget(evt);
	if(elem != null) {
		elem.style.backgroundColor = "";
	}
}

function validateAddress() {
	var formField = document.getElementById('address');
	var ok = (formField.value != null && formField.value.length != 0);
	var grpEle = document.getElementById('grpAddress');
	if(grpEle != null) {
		if(ok) {
			grpEle.className = "form-group has-success has-feedback";
			document.getElementById('addressIcon').className = "glyphicon glyphicon-ok form-control-feedback";
			document.getElementById('addressErrorMsg').innerHTML = "";
		}
		else  {
			grpEle.className = "form-group has-error has-feedback";
			document.getElementById('addressIcon').className = "glyphicon glyphicon-remove form-control-feedback";
			document.getElementById('addressErrorMsg').innerHTML = "Please enter your address";
		}
		return ok;
	}
	
}

function validateFirstName() {
	var formField = document.getElementById('firstName');
	var ok = (formField.value != null && formField.value.length != 0);
	var grpEle = document.getElementById('grpfirstName');
	if(grpEle != null) {
		if(ok) {
			grpEle.className = "form-group has-success has-feedback";
			document.getElementById('firstNameIcon').className = "glyphicon glyphicon-ok form-control-feedback";
			document.getElementById('firstNameErrorMsg').innerHTML = "";
		}
		else  {
			grpEle.className = "form-group has-error has-feedback";
			document.getElementById('firstNameIcon').className = "glyphicon glyphicon-remove form-control-feedback";
			document.getElementById('firstNameErrorMsg').innerHTML = "Please enter your first name";
		}
		return ok;
	}
}

function validateEmailAddress() {
	var formField = document.getElementById('email');
	var ok = (formField.value.length != 0 && g_reEmail.test(formField.value));
	var grpEle = document.getElementById('grpEmail');
	if(grpEle != null) {
		if(ok) {
			grpEle.className = "form-group has-success has-feedback";
			document.getElementById('EmailIcon').className = "glyphicon glyphicon-ok form-control-feedback";
			document.getElementById('emailErrorMsg').innerHTML = "";
		}
		else {
			grpEle.className = "form-group has-error has-feedback";
			document.getElementById('EmailIcon').className = "glyphicon glyphicon-remove form-control-feedback";
			document.getElementById('emailErrorMsg').innerHTML = "Please enter your valid email id";
		}
	}
	return ok;
}

function validateCellPhone() {
	var formField = document.getElementById('cellPhone');
	var ok = (formField.value.length != 0 && g_reCell.test(formField.value));
	var grpEle = document.getElementById('grpCellPhone');
	if(grpEle != null) {
		if(ok) {
			grpEle.className = "form-group has-success has-feedback";
			document.getElementById('cellPhoneIcon').className = "glyphicon glyphicon-ok form-control-feedback";
			document.getElementById('cellPhoneErrorMsg').innerHTML = "";
		}
		else {
			grpEle.className = "form-group has-error has-feedback";
			document.getElementById('cellPhoneIcon').className = "glyphicon glyphicon-remove form-control-feedback";
			document.getElementById('cellPhoneErrorMsg').innerHTML = "Please enter your valid mobile number";
		}
	}
	return ok;
}



function validateAllfields(e) {
	/* Need to do it this way to make sure all the functions execute */
	
	var bOK = validateFirstName();
	bOK &= validateEmailAddress();
    bOK &= validateCellPhone(); 
	bOK &= validateAddress(); 
	

	if(!bOK) {
		alert("The fields that are marked bold and red are required. Please supply valid\n values for these fields before sending.");
		com_abhi.EVENTS.preventDefault(e);
	}
	
	
}

com_abhi.EVENTS.addEventHandler(window, "load", function() { initFormElements("text"); }, false);

